import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
// Rutas de usuario Cliente
import Inicio from './components/Inicio.vue';
import Contacts from './components/UserClient/Contacts.vue';
import Favorites from './components/UserClient/Favorites.vue';
import Config from './components/UserClient/Config.vue';
import Service from './components/UserClient/Service.vue';
import Proveedor from './components/UserClient/Proveedor.vue';
import ProvedorAbout from './components/UserClient/ProveedorAbout.vue';
import ProvedorPackages from './components/UserClient/ProveedorPackages.vue';
import Categories from './components/UserClient/Categories.vue';
// Rutas de usuario Proveedor
import AboutProv from './components/UserProveedor/AboutProv.vue';
import ConfProv from './components/UserProveedor/ConfProv.vue';
import HistoryProv from './components/UserProveedor/HistoryProv.vue';
import PackagesProv from './components/UserProveedor/PackagesProv.vue';
import PoliticsProv from './components/UserProveedor/PoliticsProv.vue';

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Inicio',
      component: Inicio
    },
    {
      path: '/client/service',
      name: 'service',
      component: Service
    },
    {
      path: '/client/categories',
      name: 'categories',
      component: Categories
    },
    {
    path: '/client/favorites',
    name: 'favorites',
    component: Favorites
    },
    {
      path: '/client/contacts',
      name: 'contacts',
      component: Contacts
    },
    {
      path: '/client/config',
      name: 'config',
      component: Config
    },
    {
      path: '/client/proveedor',
      name: 'proveedor',
      component: Proveedor
    },
    {
      path: '/client/proveedor/about',
      name: 'proveAbout',
      component: ProvedorAbout
    },
    {
      path: '/client/proveedor/packages',
      name: 'provePackages',
      component: ProvedorPackages
    },
    // Rutas Proveedores
    {
      path: '/proveedor/about',
      name: 'proveedorAbout',
      component: AboutProv
    },
    {
      path: '/proveedor/conf',
      name: 'proveedorConf',
      component: ConfProv
    },
    {
      path: '/proveedor/history',
      name: 'proveedorHistory',
      component: HistoryProv
    },
    {
      path: '/proveedor/packages',
      name: 'proveedorPackages',
      component: PackagesProv
    },
    {
      path: '/proveedor/politics',
      name: 'proveedorPolitics',
      component: PoliticsProv
    }
  ]
})

