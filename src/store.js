import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    favorites:[
    
    ],
    services:[
      {id:0, category:"Limpieza",service:"Limpieza Radion",telefono:6188042044, email:"sirb_plus@gmail.com",puntuation:"5",description:"Servicio de limpieza profecional de domicilios particulares",disponibilidad:"10:00 AM a 6:00 PM",proveedor:"Sirvientas plus",img:"https://image.freepik.com/vector-gratis/limpiador-productos-limpieza-servicio-limpieza_18591-52057.jpg"},
      {id:1, category:"Limpieza",service:"Respect clean",telefono:6187004522, email:"red@gmail.com",puntuation:"5",description:"Servicio de limpieza que se respeta",disponibilidad:"10:00 AM a 6:00 PM",proveedor:"Sirvientas que se respetas",img:"https://image.freepik.com/vector-gratis/limpiador-productos-limpieza-servicio-limpieza_18591-52057.jpg"},
      {id:2, category:"Limpieza",service:"CLeo House",telefono:6180209467, email:"homr_msif@gmail.com",puntuation:"5",description:"Servicio de limpieza para casas",disponibilidad:"10:00 AM a 6:00 PM",proveedor:"Sirvientas de casa",img:"https://image.freepik.com/vector-gratis/limpiador-productos-limpieza-servicio-limpieza_18591-52057.jpg"},
      {id:3, category:"Limpieza",service:"CardClean",telefono:6189971012, email:"eventMaid@gmail.com",puntuation:"5",description:"Servicio de limpieza para eventos",disponibilidad:"10:00 AM a 6:00 PM",proveedor:"Sirvientas en eventos",img:"https://image.freepik.com/vector-gratis/limpiador-productos-limpieza-servicio-limpieza_18591-52057.jpg"},
      {id:4, category:"Limpieza",service:"Liempieza Gigante",telefono:6187884544, email:"industriClean@gmail.com",puntuation:"5",description:"Servicio de limpieza para empresas",disponibilidad:"10:00 AM a 6:00 PM",proveedor:"Sirvientas para empresas",img:"https://image.freepik.com/vector-gratis/limpiador-productos-limpieza-servicio-limpieza_18591-52057.jpg"},
      {id:5, category:"Limpieza",service:"Clean anniway",telefono:6181224696, email:"chafaSir@gmail.com",puntuation:"5",description:"Servicio de limpieza chafa",disponibilidad:"10:00 AM a 6:00 PM",proveedor:"Sirvienta las chafa",img:"https://image.freepik.com/vector-gratis/limpiador-productos-limpieza-servicio-limpieza_18591-52057.jpg"},
      {id:6, category:"Limpieza",service:"Limpio Geo",telefono:6184001215, email:"clanGfa@gmail.com",puntuation:"5",description:"Servicio de limpieza con tu gfa",disponibilidad:"10:00 AM a 6:00 PM",proveedor:"Sirvientas tu gfa",img:"https://image.freepik.com/vector-gratis/limpiador-productos-limpieza-servicio-limpieza_18591-52057.jpg"},
      {id:7, category:"Plomeria",service:"Plomeria sio",telefono:6180364784, email:"gimboPlom@gmail.com",puntuation:"5",description:"Te destapo el caño XD",disponibilidad:"10:00 AM a 6:00 PM",proveedor:"Plomeria Gimbo",img:"https://i0.pngocean.com/files/38/832/849/cartoon-plumber.jpg"},
      {id:8, category:"Plomeria",service:"Plomeria hasta tu casa",telefono:618471048, email:"juanitosLoc@gmail.com",puntuation:"5",description:"vamos a tu casa a destaparte la coladera!!",disponibilidad:"10:00 AM a 6:00 PM",proveedor:"Don juan",img:"https://i0.pngocean.com/files/38/832/849/cartoon-plumber.jpg"}
    ],
    categories:[
      {id:0, category:"Limpieza",img:"https://image.freepik.com/vector-gratis/limpiador-productos-limpieza-servicio-limpieza_18591-52057.jpg"},
      {id:1, category:"Plomeria",img:"https://i0.pngocean.com/files/38/832/849/cartoon-plumber.jpg"},
      {id:2, category:"Banquetes",img:"https://cdn1.vectorstock.com/i/1000x1000/51/70/girl-and-healthy-food-cartoon-vector-10495170.jpg"},
      {id:3, category:"Electricidad",img:"https://images.locanto.com.co/3924835533/SERVICIO-ELECTRICO-DOMICILIARIO_1.png"},
      {id:4, category:"Carpinteria",img:"https://st2.depositphotos.com/1985863/6221/v/950/depositphotos_62213987-stock-illustration-carpenter-cartoon-illustration.jpg"},
      {id:5, category:"Meseros",img:"http://www.meserosonline.com/assets/img/meseros-1.png"},
      {id:6, category:"Electronica",img:"https://thumbs.dreamstime.com/b/computer-chip-technology-processor-circuit-motherboard-information-system-isometric-flat-d-isolated-concept-119223339.jpg"},
      {id:7, category:"Celulares",img:"https://us.123rf.com/450wm/spiralmedia/spiralmedia1310/spiralmedia131000006/22800737-caricatura-de-un-hombre-de-negocios-con-aplicaciones-para-smartphone-.jpg?ver=6"},
      {id:8, category:"Musica",img:"http://2.bp.blogspot.com/-e3j2VLA1Tqs/VVuvQVjQd2I/AAAAAAAAAJY/wwNQV5nsKiE/s1600/6726569a8aea68fa8f3ac59f63d1567b.jpg.gif"}
    ],
    proveedorPackages: [
      {idServ:0, id:0, idUser:2, paquete:"Limp Maid 1",description:"Limpiamos tu casa, en un dos por 3",incluye:"produtos de limpieza + 1 trabajador + cubre 4 habitaciones",precio:350},
      {idServ:0, id:1, idUser:2, paquete:"Limp Maid 2",description:"Necesitas limpiar bastante, limpiamos por pisos!!!",incluye:"produtos de limpieza + 2 trabajadores + cubre 1 piso",precio:800},
      {idServ:0, id:2, idUser:2, paquete:"Limp Maid 3",description:"Vives en la suciedad y necesitas limpiar toda tu casa",incluye:"produtos de limpieza + 3 trabajadores + cubre 2 pisos o más",precio:1300},
      {idServ:1, id:0, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:1, id:1, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:2, id:0, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:2, id:1, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:3, id:0, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:3, id:1, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:4, id:0, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:4, id:1, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:5, id:0, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:5, id:1, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:6, id:0, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:6, id:1, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:7, id:0, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:7, id:1, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:8, id:0, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:8, id:1, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:9, id:0, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:9, id:1, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:10, id:0, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:10, id:1, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35},
      {idServ:10, id:2, paquete:"Taquitos dorados",description:"Tambien se llaman flautas?",incluye:"orden de tacos dorados",precio:35}
    ],
    users:[
      {id:0,name:"Andree",paterno:"Galvan",materno:"Candia",pass:"123",mail:"roket432@hotmail.com",telefono:"6183009845",address:"Colonia Hipodromo, Calle de Las américas 123-A",status:1,type:1,statusProv:1},
      {id:1,name:"Perla",paterno:"Alarcon",materno:"Lopez",pass:"123",mail:"perlita@hotmail.com",telefono:"6182259870",address:"Colonia Hipodromo, Calle de Las américas 124-A",status:1,type:0,statusProv:1},
      {id:2,name:"Jesus",paterno:"Santos",materno:"Flores",pass:"123",mail:"chuny@hotmail.com",telefono:"6180304884",address:"Colonia Hipodromo, Calle de Las américas 125-A",status:1,type:0,statusProv:0},
      {id:3,name:"Ulises",paterno:"Manrique",materno:"Flores",pass:"123",mail:"lilchamp@hotmail.com",telefono:"6187893030",address:"Colonia Hipodromo, Calle de Las américas 126-A",status:1,type:0,statusProv:1}
    ],
    contacts:[
      
    ],
    
    carrouselImgs: [
      {
        src: 'https://conceptodefinicion.de/wp-content/uploads/2016/06/Marketing_de_Servicios.jpg',
      },
      {
        src: 'https://dalealaweb.com/wp-content/uploads/2015/11/Servicios-Adicionales-Agencias-Digitales-Profesionales-PB.png',
      },
      {
        src: 'https://www.pymesycalidad20.com/wp-content/uploads/2018/01/Servicios-calidad.png',
      },
      {
        src: 'https://www.foromarketing.com/wp-content/uploads/2011/11/marketing-de-servicios.png',
      },
    ],
    islogued:false,
    logid:null,
    itemID:null,
    arrService: null,
    idService: null,
    arrPack: null,
    arrCont: null,
    arrFav: null,
    idFav: 0,
    idCon: 0
  },
  mutations: {
    login (state,id) {
      state.islogued=!state.islogued
      state.logid=id
    },
    register(state,arr){
      state.users.push(arr);
    },
    resId (state,id){
      state.itemID=id
    },
    createServicesAr(state, cat){
      state.arrService = cat
    },
    createPackAr(state, pack){
      state.arrPack = pack
    },
    createConArr(state,arr){
      state.arrCont = arr
    },
    createFavArr(state,arr){
      state.arrFav = arr
    },
    toFav(state, itemId) {
      state.idFav++;
      state.favorites.push({
        idFav: state.idFav,
        idServ: itemId,
        idUser: state.logid,
        category: state.services[itemId].category,
        service: state.services[itemId].service,
        description: state.services[itemId].description,
        disponibilidad: state.services[itemId].disponibilidad,
        proveedor: state.services[itemId].proveedor,
        img: state.services[itemId].img
      });
    },
    toCont(state, item) {
      state.idCon++;
      state.contacts.push({
        idContact: state.idCon,
        service: state.services[item.idServ].service,
        contacto: state.services[item.idServ].proveedor,
        telefono: state.services[item.idServ].telefono,
        email: state.services[item.idServ].email,
        paquete: item.paquete,
        description: item.description,
        incluye: item.incluye,
        pago: item.precio,
        idUser: state.logid
      });
    },
    delCont(state, indx) {
      state.contacts.splice(indx, 1);
    },
  },
  actions: {

  },
  getters: {

  }
})

